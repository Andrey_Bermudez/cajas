<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ValidacionSale;
use App\Models\Admin\Articles;
use App\Models\Admin\Person;
use App\Models\Admin\Sale;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request){
            $query = trim($request->get('search'));
            $articles = Articles::all();
            $persons = Person::all();
            $monto_t_v = DB::select('SELECT sum(amount) AS total FROM sale');
            $datas = Sale::where('article_id', 'LIKE', '%' . $query . '%')
            ->orderBy('id', 'asc')
            ->paginate(8);
            return view('admin.sale.index', ['datas' => $datas, 'search' => $query, 'articles' => $articles, 'monto_t_v' => $monto_t_v, 'persons' => $persons]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function crear()
    {
        $fecha = Carbon::now();
        $articles = Articles::all();
        $persons = Person::all();
        $datas = Sale::all();
        return view('admin.sale.crear', compact('datas','articles','fecha', 'persons'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function guardar(ValidacionSale $request)
    {   
        $articulo = $request->article_id;//Articulo seleccionado
        $cantidad_i = $request->quantity;
        $precios = DB::select('SELECT price AS price FROM articles WHERE articles.id = '.$articulo);
        foreach($precios as $precio)
        $precio_m = $precio->price;
        $precio_d = $cantidad_i * $precio_m;
        $cantidades = DB::select('SELECT quantity AS quantity FROM articles WHERE articles.id = '.$articulo);
        foreach($cantidades as $cantidad)
        $cantidad_actual = $cantidad->quantity;
        if($cantidad_i <= $cantidad_actual){
            DB::update('UPDATE articles SET quantity = quantity - '.$cantidad_i.' WHERE articles.id = '.$articulo);
            Sale::create($request->all());
            $sales = Sale::all();
            foreach($sales as $sale)
            $v_i = $sale->id;
            DB::update('UPDATE sale SET sale.amount = '.$precio_d.' WHERE sale.id = '.$v_i);
            return redirect('admin/sale')->with('mensaje', 'Venta realizada con exito');     
        }else{
            return redirect('admin/sale/crear')->with('mensaje-warning', 'La cantidad se a pasado, ya no hay más articulos');
        }
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editar($id)
    {
        $fecha = Carbon::now();
        $data = Sale::findOrFail($id);
        return view('admin.sale.editar', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function actualizar(Request $request, $id)
    {
        Sale::findOrFail($id)->update($request->all());
        return redirect('admin/sale')->with('mensaje-info', 'Venta actualizada con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function eliminar($id)
    {
        Sale::find($id)->delete();
        return redirect('admin/sale')->with('mensaje-alert','Venta eliminada satisfactoriamente');
    }
}