<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ValidacionDay;
use App\Models\Admin\Day;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fecha = Carbon::now();
        $datas = Day::orderBy('id')->get();
        return view('admin.day.index', compact('datas','fecha'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function crear()
    {
        $fecha = Carbon::now();
        $datas = Day::all();
        return view('admin.day.crear', compact('datas','fecha'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function guardar(ValidacionDay $request)
    {
        Day::create($request->all());
        return redirect('admin/day')->with('mensaje', 'Dia creado con exito');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editar($id)
    {
        $fecha = Carbon::now();
        $data = Day::findOrFail($id);
        return view('admin.day.edit', compact('data','fecha'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function actualizar(Request $request, $id)
    {
        Day::findOrFail($id)->update($request->all());
        return redirect('admin/day')->with('mensaje', 'Dia actualizado con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function eliminar($id)
    {
        Day::find($id)->delete();
        return redirect('admin/day')->with('mensaje', 'Dia eliminado satisfactoriamente');
    }
}
