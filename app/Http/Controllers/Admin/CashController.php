<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ValidacionCash;
use App\Models\Admin\Cash;
use App\Models\Admin\Person;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class CashController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request){
            $query = trim($request->get('search'));
            $fecha = Carbon::now();
            $cashs = Cash::all();
            foreach($cashs as $cash)
            $ca = $cash->id;
            $persons = Person::all();
            $vtr = DB::select('SELECT sum(package_1 + package_2 + package_3 + package_4 + package_5 + package_6 + box_total + credit + b_bn
            + b_cr + b_c + b_f + b_v + dollars + checks + available + token) AS total_v_r FROM cash WHERE cash.id = '.$ca);
            foreach($vtr as $v)
            $a = $v->total_v_r;
            $caja_anterior = DB::select('SELECT previous_box AS anterior FROM cash WHERE cash.id = '.$ca);
            $combustible = DB::select('SELECT fuel_sale AS venta_com FROM cash WHERE cash.id = '.$ca);
            foreach($caja_anterior as $caj)
            $c = $caj->anterior;
            $total = $a-$c;
            foreach($combustible as $com)
            $f = $com->venta_com;
            $diferencia = ($total - $f);
            DB::update("update cash set difference = $diferencia where id = $ca");
            $datas = Cash::where('date', 'LIKE', '%' . $query . '%')
            ->orderBy('id', 'asc')
            ->paginate(8);
            return view('admin.cash.index', ['datas' => $datas, 'searc' => $query, 'fecha' => $fecha, 'vtr' => $vtr, 'caja_anterior' => $caja_anterior, 'combustible' => $combustible, 'persons' => $persons]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function crear()
    {
        $fecha = Carbon::now();
        $persons = Person::all();
        return view('admin.cash.crear', compact('persons','fecha'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function guardar(ValidacionCash $request)
    {   
        Cash::create($request->all());
        return redirect('admin/cash')->with('mensaje', 'Caja creada con exito');
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editar($id)
    {
        $fecha = Carbon::now();
        $data = Cash::findOrFail($id);
        return view('admin.cash.editar', compact('data','fecha'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function actualizar(ValidacionCash $request, $id)
    {
        Cash::findOrFail($id)->update($request->all());
        Cash::all();
        Person::all();
        $vtr = DB::select('SELECT sum(package_1 + package_2 + package_3 + package_4 + package_5 + package_6 + box_total + credit + b_bn
        + b_cr + b_c + b_f + b_v + dollars + checks + available + token) AS total_v_r FROM cash WHERE cash.id = '.$id);
        foreach($vtr as $v)
        $a = $v->total_v_r;
        $caja_anterior = DB::select('SELECT previous_box AS anterior FROM cash WHERE cash.id = '.$id);
        $combustible = DB::select('SELECT fuel_sale AS venta_com FROM cash WHERE cash.id = '.$id);
        foreach($caja_anterior as $caj)
        $c = $caj->anterior;
        $total = $a-$c;
        foreach($combustible as $com)
        $f = $com->venta_com;
        $diferencia = ($total - $f);
        DB::update("update cash set difference = $diferencia where id = $id");
        return redirect('admin/cash')->with('mensaje-info', 'Caja actualizada con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function eliminar($id)
    {
        Cash::find($id)->delete();
        return redirect('admin/cash')->with('mensaje-alert','Caja eliminada satisfactoriamente');
    }
}