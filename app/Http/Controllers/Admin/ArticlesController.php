<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ValidacionArticles;
use App\Models\Admin\Articles;
use App\Models\Admin\Brand;
use Illuminate\Http\Request;

class ArticlesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request){
            $query = trim($request->get('search'));
            $brands = Brand::all();
            $datas = Articles::where('name', 'LIKE', '%' . $query . '%')
            ->orderBy('id', 'asc')
            ->paginate(8);
            return view('admin.article.index', ['datas' => $datas, 'search' => $query, 'brands' => $brands]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function crear()
    {
        $brands = Brand::all();
        $datas = Articles::all();
        return view('admin.article.crear', compact('datas','brands'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function guardar(ValidacionArticles $request)
    {   
        Articles::create($request->all());
        return redirect('admin/article')->with('mensaje', 'Articulo creado con exito');
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editar($id)
    {
        $data = Articles::findOrFail($id);
        return view('admin.article.editar', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function actualizar(ValidacionArticles $request, $id)
    {
        Articles::findOrFail($id)->update($request->all());
        return redirect('admin/article')->with('mensaje-info', 'Articulo actualizado con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function eliminar($id)
    {
        Articles::find($id)->delete();
        return redirect('admin/article')->with('mensaje-alert','Articulo eliminado satisfactoriamente');
    }
}