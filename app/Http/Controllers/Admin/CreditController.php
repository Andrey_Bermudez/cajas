<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ValidacionCredit;
use App\Models\Admin\Credit;
use App\Models\Admin\Person;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class CreditController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request){
            $query = trim($request->get('search'));
            $fecha = Carbon::now();
            $persons = Person::all();
            $monto = DB::select('SELECT sum(amount) AS total FROM credit WHERE credit.state = "pendiente"');
            $datas = Credit::where('date', 'LIKE', '%' . $query . '%')
            ->orderBy('id', 'asc')
            ->paginate(8);
            return view('admin.credit.index', ['datas' => $datas, 'search' => $query, 'fecha' => $fecha, 'persons' => $persons, 'monto' => $monto]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function crear()
    {
        $fecha = Carbon::now();
        $persons = Person::all();
        return view('admin.credit.crear', compact('persons','fecha'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function guardar(ValidacionCredit $request)
    {   
        $person = $request->person_id; //Se toma el valor del monto que se ingresa en el formulario
        $monto = DB::select('SELECT sum(amount) AS total FROM credit WHERE credit.state = "pendiente" AND credit.person_id = '.$person);//Se atrae la suma de todos los montos dependiendo del cliente que se elige
        foreach($monto as $mont)
            $total = $mont->total;//Se saca el total en tipo float

        $total_global = $total + $request->amount;//Se hace la suma del total que se tiene de credito actual, más lo que se esta agregando ahora   

        $limites = DB::select('SELECT limit_credit AS limite FROM person WHERE person.id = '.$person);//Se atrae el limite dependiendo del cliente que se elige
        foreach($limites as $limite)
            $total_limite = $limite->limite;//Se saca el limite en tipo float
        if($request->amount <= 990000 && $total_global <= $total_limite){
                Credit::create($request->all());
                return redirect('admin/credit')->with('mensaje', 'Credito creado con exito');
            }else{  
            return redirect('admin/credit/crear')->with('mensaje-warning', 'Advertencia el monto maximo es de 990000, O su credito a llegado al limite');
        }
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editar($id)
    {
        $fecha = Carbon::now();
        $data = Credit::findOrFail($id);
        if($data->state == "Pendiente"){
            return view('admin.credit.editar', compact('data','fecha'));
        }else{
            return redirect('admin/credit')->with('mensaje-info', 'El credito esta cancelado');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function actualizar(Request $request, $id)
    {
        Credit::findOrFail($id)->update($request->all());
        return redirect('admin/credit')->with('mensaje-info', 'Credito cancelado con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function eliminar($id)
    {
        Credit::find($id)->delete();
        return redirect('admin/credit')->with('mensaje-alert','Credito anulado satisfactoriamente');
    }
}