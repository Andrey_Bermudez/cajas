<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\Credit;
use App\Models\Admin\Person;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class C_CreditController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $person = $request->person_id;
        $fecha = Carbon::now();
        $persons = Person::all();
        $datas = Credit::orderBy('id')->where('person_id', '=' , session()->get('person_id'))->get(); 
        $monto = DB::select('SELECT sum(amount) AS total FROM credit WHERE credit.state = "pendiente" AND credit.person_id = '.session()->get('person_id'));//Se atrae la suma de todos los montos dependiendo del cliente que se elige
        return view('admin.c_credit.index', compact('datas', 'persons','fecha', 'monto'));
    }
}