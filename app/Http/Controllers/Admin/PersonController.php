<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ValidacionPerson;
use App\Models\Admin\Person;
use App\Models\Admin\Role;
use Illuminate\Http\Request;

class PersonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request){
            $query = trim($request->get('search'));
            $roles = Role::all();
            $datas = Person::where('full_name', 'LIKE', '%' . $query . '%')
            ->orderBy('id', 'asc')
            ->paginate(10);
            return view('admin.person.index', ['datas' => $datas, 'search' => $query, 'roles' => $roles]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function crear()
    {
        $roles = Role::all();
        return view('admin.person.crear', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function guardar(ValidacionPerson $request)
    {
        Person::create($request->all());
        return redirect('admin/person')->with('mensaje', 'Persona creada con exito');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editar($id)
    {
        $roles = Role::all();
        $data = Person::findOrFail($id);
        return view('admin.person.editar', compact('data', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function actualizar(ValidacionPerson $request, $id)
    {
        Person::findOrFail($id)->update($request->all());
        return redirect('admin/person')->with('mensaje-info', 'Persona actualizada con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function eliminar(Request $request, $id)
    {
        Person::find($id)->delete();
        return redirect('admin/person')->with('mensaje-alert','Persona eliminada satisfactoriamente');
    }
}