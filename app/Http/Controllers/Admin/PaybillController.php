<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ValidacionPaybill;
use App\Models\Admin\Paybill;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PaybillController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fecha = Carbon::now();
        $datas = Paybill::orderBy('id')->get();
        return view('admin.paybill.index', compact('datas','fecha'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function crear()
    {
        $fecha = Carbon::now();
        $datas = Paybill::all();
        return view('admin.paybill.crear', compact('datas','fecha'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function guardar(ValidacionPaybill $request)
    {
        Paybill::create($request->all());
        return redirect('admin/paybill')->with('mensaje', 'Vale creado con exito');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editar($id)
    {
        $fecha = Carbon::now();
        $data = Paybill::findOrFail($id);
        if($data->state == "Pendiente"){
            return view('admin.paybill.editar', compact('data','fecha'));
        }else{
            return redirect('admin/paybill')->with('mensaje-info', 'El vale esta cancelado');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function actualizar(Request $request, $id)
    {
        Paybill::findOrFail($id)->update($request->all());
        return redirect('admin/paybill')->with('mensaje-info', 'Vale actualizado con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function eliminar($id)
    {
        Paybill::find($id)->delete();
        return redirect('admin/paybill')->with('mensaje-alert','Vale eliminado satisfactoriamente');
    }
}
