<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\Articles;
use App\Models\Admin\Cash;
use App\Models\Admin\Person;
use App\Models\Admin\Role;
use App\Models\Admin\Sale;
use Barryvdh\DomPDF\Facade as PDF;

class PDFController extends Controller
{
    public function PDFPerson()
    {
        $datas = Person::all();
        $roles = Role::all();
        $pdf = PDF::loadView('person-pdf', compact('datas', 'roles'));
        return $pdf->stream('personas.pdf');
    }

    public function PDFSale()
    {
        $articles = Articles::all();
        $datas = Sale::all();
        $pdf = PDF::loadView('sale-pdf', compact('articles', 'datas'));
        return $pdf->stream('sales.pdf');
    }

    public function PDFCash()
    {
        $persons = Person::all();
        $datas = Cash::all();
        $pdf = PDF::loadView('cash-pdf', compact('persons', 'datas'));
        return $pdf->stream('cashes.pdf');
    }
}