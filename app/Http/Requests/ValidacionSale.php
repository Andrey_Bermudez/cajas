<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidacionSale extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date' => 'required',
            'article_id' => 'required', 
            'quantity' => 'required', 
            'nombre_person' => 'required', 
        ];
    }
    public function messages()
    {
        return [
            'quantity.required' => 'El campo Cantidad es requerido.',
        ];
    }
}
