<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidacionArticles extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => 'required | numeric |unique:articles,code,' .$this->route('id'),
            'name' => 'required', 
            'quantity' => 'required', 
            'price' => 'required | numeric', 
            'brand_id' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'code.required' => 'El campo Código es obligatorio.',
            'code.numeric' => 'El campo Código solo recibe numeros.',
            'code.unique' => 'El Código ya existe.',
            'name.required' => 'El campo Nombre es obligatorio.',
            'price.required' => 'El campo Precio es obligatorio.',
            'price.numeric' => 'El campo Precio solo recibe numeros.',
        ];
    }
}