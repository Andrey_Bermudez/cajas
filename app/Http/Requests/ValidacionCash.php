<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidacionCash extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id',
            'cashier_id' => 'required',
            'date' => 'required',
            'package_1' => 'numeric', 
            'package_2' => 'numeric', 
            'package_3' => 'numeric', 
            'package_4' => 'numeric', 
            'package_5' => 'numeric', 
            'package_6' => 'numeric', 
            'box_total' => 'numeric', 
            'credit' => 'numeric', 
            'b_bn' => 'numeric', 
            'b_cr' => 'numeric', 
            'b_c' => 'numeric', 
            'b_f' => 'numeric', 
            'b_v' => 'numeric', 
            'dollars' => 'numeric', 
            'checks' => 'numeric', 
            'available' => 'numeric', 
            'token' => 'numeric', 
            'previous_box' => 'numeric', 
            'fuel_sale' => 'numeric',
            'difference' => 'numeric',
        ];
    }	
    public function messages()
    {
        return [
            'package_1.numeric' => 'El campo Deposito #1 solo recibe numeros.',
            'package_2.numeric' => 'El campo Deposito #2 solo recibe numeros.',
            'package_3.numeric' => 'El campo Deposito #3 solo recibe numeros.',
            'package_4.numeric' => 'El campo Deposito #4 solo recibe numeros.',
            'package_5.numeric' => 'El campo Deposito #5 solo recibe numeros.',
            'package_6.numeric' => 'El campo Deposito #6 solo recibe numeros.',
            'box_total.numeric' => 'El campo Caja Total solo recibe numeros.',
            'credit.numeric' => 'El campo Credito solo recibe numeros.',
            'b_bn.numeric' => 'El campo Banco Nacional solo recibe numeros.',
            'b_cr.numeric' => 'El campo Banco Costa Rica solo recibe numeros.',
            'b_c.numeric' => 'El campo Credomatic solo recibe numeros.',
            'b_f.numeric' => 'El campo Flota solo recibe numeros.',
            'b_v.numeric' => 'El campo Versatec solo recibe numeros.',
            'dollars.numeric' => 'El campo Dólares solo recibe numeros.',
            'checks.numeric' => 'El campo Cheques solo recibe numeros.',
            'available.numeric' => 'El campo Disponibles solo recibe numeros.',
            'token.numeric' => 'El campo Vales solo recibe numeros.',
            'previous_box.numeric' => 'El campo Caja Anterior solo recibe numeros.',
            'fuel_sale.numeric' => 'El campo Venta de Combustible solo recibe numeros.',
        ];
    }
}
