<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidacionPaybill extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'client' => 'required |unique:paybill,client,' .$this->route('id'),
            'client' => 'regex:/^[\pL\s\-]+$/u',
            'amount' => 'required | numeric',
            'description' => 'required',
            'state' => 'required',
            'date' => 'required',
            'nombre_person' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'client.regex' => 'El Campo Cliente solo recibe letras.',
            'amount.required' => 'El campo Monto es obligatorio.',
            'amount.numeric' => 'El campo Monto solo recibe numeros.',
            'description.required' => 'El campo Descripcion es requerido.',
        ];
    }
}
