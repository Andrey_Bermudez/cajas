<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidacionPerson extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user' => 'required |unique:person,user,' .$this->route('id'),
            'password' => 'required',
            'full_name' => 'required| regex:/^[\pL\s\-]+$/u',
            'n_telephone' => 'required | numeric',
            'mail' => 'required',
            'address' => 'required',
            'limit_credit' => 'numeric',
            'role_id' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'user.required' => 'El campo Usuario es requerido.',
            'user.unique' => 'El Usuario ya existe.',
            'limit_credit.numeric' => 'El campo Limite de credito solo recibe numeros.',
            'password.required' => 'El campo Contraseña es requerido.',
            'full_name.regex' => 'El Campo Nombre Completo solo recibe letras.',
            'full_name.required' => 'El campo Nombre Completo es requerido.',
            'n_telephone.required' => 'El campo Telefono es requerido.',
            'n_telephone.numeric' => 'El campo Telefono solo recibe numeros.',
            'mail.required' => 'El campo Correo Electronico es requerido.',
            'address.required' => 'El campo Direccion es requerido.',
        ];
    }
}
