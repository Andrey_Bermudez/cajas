<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Articles extends Model
{
    protected $table = "articles";
    protected $fillable = ['code', 'name', 'quantity', 'price', 'brand_id'];
    protected $guarded = ['id'];
}
