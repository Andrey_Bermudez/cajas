<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    protected $table = "sale";
    protected $fillable = ['date', 'article_id', 'quantity', 'amount', 'nombre_person'];
    protected $guarded = ['id'];
}
