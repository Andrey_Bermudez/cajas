<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Paybill extends Model
{
    protected $table = "paybill";
    protected $fillable = ['client','amount', 'description', 'state', 'date', 'nombre_person'];
    protected $guarded = ['id'];
}
