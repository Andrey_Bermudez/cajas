<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Permit extends Model
{
    protected $table = "permit";
    protected $fillable = ['name', 'slug'];
    protected $guarded = ['id'];
}
