<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Credit extends Model
{
    protected $table = "credit";
    protected $fillable = ['person_id', 'amount', 'description', 'state', 'date', 'nombre_person'];
    protected $guarded = ['id'];
}
