<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Cash extends Model
{
    protected $table = "cash";
    protected $fillable = ['cashier_id', 'date', 'package_1', 'package_2', 'package_3', 'package_4', 'package_5', 'package_6', 
    'box_total', 'credit', 'b_bn', 'b_cr', 'b_c', 'b_f', 'b_v', 'dollars', 'checks', 'available', 'token', 'previous_box', 'fuel_sale', 'difference'];
    protected $guarded = ['id'];
}