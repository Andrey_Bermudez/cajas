<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Day extends Model
{
    protected $table = "day";
    protected $fillable = ['date'];
    protected $guarded = ['id'];
}