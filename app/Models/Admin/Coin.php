<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Coin extends Model
{
    protected $table = "coin";
    protected $fillable = ['name', 'symbol', 'type_change'];
    protected $guarded = ['id'];
}