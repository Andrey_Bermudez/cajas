<?php

namespace App\Models\Seguridad;

use App\Models\Admin\Role;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;

class Person extends Authenticatable
{
    protected $remenber_token = false;
    protected $table = 'person';
    protected $fillable = ['user', 'password', 'full_name', 'n_telephone', 'mail', 'address', 'limit_credit', 'role_id'];
    protected $guarded = ['id'];

    public function setSession(){
        Session::put(
            [
                'usuario' => $this->user,
                'person_id' => $this->id,
                'nombre_person' => $this->full_name,
                'role_id' => $this->role_id,
            ]
        );
    }

    public function setPasswordAttribute($pass){
        $this->attributes['password'] = Hash::make($pass);
    }
}
