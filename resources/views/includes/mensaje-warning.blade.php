@if(session("mensaje-warning"))
    <div class="alert alert-warning alert-dismissible" data-auto-dismiss="3000">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-warning"></i> Mensaje sistema Cajas</h4>
        <ul>
            <li>{{ session("mensaje-warning") }}</li>
        </ul>
    </div>
@endif