@if(session("mensaje-info"))
    <div class="alert alert-info alert-dismissible" data-auto-dismiss="3000">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-info"></i> Mensaje sistema Cajas</h4>
        <ul>
            <li>{{ session("mensaje-info") }}</li>
        </ul>
    </div>
@endif