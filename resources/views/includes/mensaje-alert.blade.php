@if(session("mensaje-alert"))
    <div class="alert alert-danger alert-dismissible" data-auto-dismiss="3000">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-times"></i> Mensaje sistema Cajas</h4>
        <ul>
            <li>{{ session("mensaje-alert") }}</li>
        </ul>
    </div>
@endif