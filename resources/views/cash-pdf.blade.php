<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Lista de Cajas</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>
    <img src="../img/esp.jpg" alt="Logo Estacion">
    <style>
        h3{
            text-align: center;
        }
    </style>
    <h3>Lista de Cajas</h3>
    <div class="container">
        <table class="table table-striped">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Fecha</th>
                    <th scope="col">Cajero</th>
                    <th scope="col">Caja Total</th>
                    <th scope="col">Diferencia</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($datas as $data)
                <tr>
                    <td>{{$data->id}}</td>
                    <td>{{$data->date}}</td>
                    <td>
                        @foreach ($persons as $person)
                            @if($data->cashier_id == $person->id)
                                {{$person->full_name}}
                            @endif
                        @endforeach
                    </td>
                    <td>{{$data->box_total}}</td>
                    <td>
                        {{number_format($data->difference)}}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</body>
</html>