<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Lista de Ventas</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>
    <img src="../img/esp.jpg" alt="Logo Estacion">
    <style>
        h3{
            text-align: center;
        }
    </style>
    <h3>Lista de Ventas</h3>
    <div class="container">
        <table class="table table-striped">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">Fecha</th>
                    <th scope="col">Articulo</th>
                    <th scope="col">Cantidad</th>
                    <th scope="col">Monto</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($datas as $data)
                <tr>
                    <td>{{$data->date}}</td>
                    <td>
                        @foreach ($articles as $article)
                            @if($data->article_id == $article->id)
                                {{$article->name}}
                            @endif
                        @endforeach
                    </td>
                    <td>{{$data->quantity}}</td>
                    <td>{{number_format($data->amount)}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</body>
</html>