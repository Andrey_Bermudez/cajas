@extends("theme.$theme.layout")
@section('titulo')
Mis Creditos
@endsection

@section("scripts")
<script src="{{asset("assets/pages/scripts/admin/index.js")}}" type="text/javascript"></script>
@endsection

@section('contenido')
<div class="row">
    <div class="col-lg-12">
        @include('includes.mensaje')
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Mis Creditos</h3>
            </div>
            <div class="box-body">
                <table class="table table-striped table-bordered table-hover" id="tabla-data">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Monto</th>
                            <th>Descripcion</th>
                            <th>Estado</th>
                            <th>Fecha</th>
                            <th>Usuario</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($datas as $data)
                        <tr>
                            <td>
                                @foreach ($persons as $person)
                                    @if($data->person_id == $person->id)
                                        {{$person->full_name}}
                                    @endif
                                @endforeach
                            </td>
                            <td>{{number_format($data->amount)}}</td>
                            <td>{{$data->description}}</td>
                            <td>{{$data->state}}</td>
                            <td>{{$data->date}}</td>
                            <td>{{$data->nombre_person}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                    <td>
                        @foreach ($monto as $mont)
                        Monto Total: {{number_format($mont->total)}}
                        @endforeach
                    </td>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection 