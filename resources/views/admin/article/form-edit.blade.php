<div class="form-group">
    <label for="code" class="col-lg-3 control-label requerido">Codigo</label>
    <div class="col-lg-8">
      <input type="text" name="code" id="code" class="form-control" readonly="readonly" value="{{old('code', $data->code ?? '')}}" required/>
    </div>
</div>
<div class="form-group">
    <label for="name" class="col-lg-3 control-label requerido">Articulo</label>
    <div class="col-lg-8">
      <input type="text" name="name" id="name" class="form-control" readonly="readonly" value="{{old('name', $data->name ?? '')}}" required/>
    </div>
</div>
<div class="form-group">
    <label for="quantity" class="col-lg-3 control-label">Cantidad</label>
    <div class="col-lg-8">
      <input type="number" name="quantity" id="quantity" class="form-control" min="0" max="25" value="{{old('quantity', $data->quantity ?? '')}}" required/>
    </div>
</div>
<div class="form-group">
    <label for="price" class="col-lg-3 control-label">Precio</label>
    <div class="col-lg-8">
      <input type="text" name="price" id="price" class="form-control" value="{{old('price', $data->price ?? '')}}" required/>
    </div>
</div>
<div class="form-group">
    <label for="brand_id" class="col-lg-3 control-label requerido">Proveedor</label>
    <div class="col-lg-8">
      <input type="text" name="brand_id" id="brand_id" class="form-control" readonly="readonly" value="{{old('brand_id', $data->brand_id ?? '')}}" required/>
    </div>
</div>