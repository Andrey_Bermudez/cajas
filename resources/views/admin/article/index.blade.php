@extends("theme.$theme.layout")
@section('titulo')
Articulos
@endsection

@section("scripts")
<script src="{{asset("assets/pages/scripts/admin/index.js")}}" type="text/javascript"></script>
@endsection

@section('contenido')
<div class="row">
    <div class="col-lg-12">
        @include('includes.mensaje')
        @include('includes.mensaje-info')
        @include('includes.mensaje-alert')
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Articulos</h3>
                <div class="box-tools pull-right">
                    <a href="{{route('crear_article')}}" class="btn btn-block btn-success btn-sm">
                        <i class="fa fa-fw fa-plus-circle"></i> Nuevo articulo
                    </a>
                </div>
            </div>
            <form class="form-inline float-right">
                <div class="input-group input-group-sm">
                    <input class="form-control" name="search" type="search" placeholder="Buscar Articulo" aria-label="Search">
                </div>
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Buscar</button>
            </form>
            <div class="box-body">
                <table class="table table-striped table-bordered table-hover" id="tabla-data">
                    <thead>
                        <tr>
                            <th>Codigo</th>
                            <th>Nombre</th>
                            <th>Cantidad</th>
                            <th>Precio</th>
                            <th>Proveedor</th>
                            <th class="width78"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($datas as $data)
                        <tr>
                            <td>{{$data->code}}</td>
                            <td>{{$data->name}}</td>
                            <td>{{$data->quantity}}</td>
                            <td>{{number_format($data->price)}}</td>
                            <td>
                                @foreach ($brands as $brand)
                                    @if($data->brand_id == $brand->id)
                                        {{$brand->name}}
                                    @endif
                                @endforeach
                            </td>
                            <td>
                                <a href="{{route('editar_article', ['id' => $data->id])}}" class="btn-accion-tabla tooltipsC" title="Editar este registro">
                                    <i class="fa fa-fw fa-pencil"></i>
                                </a>
                                <form action="{{route('eliminar_article', ['id' => $data->id])}}" class="d-inline form-eliminar" method="POST">
                                    @csrf @method("delete")
                                    <button type="submit" class="btn-accion-tabla eliminar tooltipsC" title="Eliminar este articulo">
                                        <i class="fa fa-fw fa-trash text-danger"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $datas->links() }}
            </div>
        </div>
    </div>
</div>
@endsection 