<div class="form-group">
    <label for="date" class="col-lg-3 control-label">Fecha</label>
    <div class="col-lg-8">
      <input type="date" name="date" id="date" class="form-control" readonly="readonly" value="{{old('date', $fecha->format('Y-m-d'), $data->date ?? '')}}"/>
    </div>
  </div>
<div class="form-group">
    <label for="article_id" class="col-lg-3 control-label requerido">Articulo</label>
    <div class="col-lg-8">
      <select name="article_id" id="article_id" class="form-control" required>
        <option value="">Seleccione el Articulo</option>
        @foreach ($articles as $article)
            <option value="{{old('article_id',$article->id ?? '')}}">{{$article->name}}</option>
        @endforeach
      </select>
    </div>
</div>
<div class="form-group">
  <label for="quantity" class="col-lg-3 control-label">Cantidad</label>
  <div class="col-lg-8">
    <input type="number" name="quantity" id="quantity" class="form-control" min="1" max="10" placeholder="1-10" value="{{old('quantity', $data->quantity ?? '')}}"/>
  </div>
</div>
<div class="form-group">
  <label for="nombre_person" class="col-lg-3 control-label">Usuario</label>
  <div class="col-lg-8">
    <input type="text" name="nombre_person" id="nombre_person" class="form-control" readonly="readonly" value="{{old('nombre_person', session()->get('nombre_person'), $data->nombre_person ?? 'Invitado')}}"/>
  </div>
</div>
