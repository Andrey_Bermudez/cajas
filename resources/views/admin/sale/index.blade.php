@extends("theme.$theme.layout")
@section('titulo')
Ventas
@endsection

@section("scripts")
<script src="{{asset("assets/pages/scripts/admin/index.js")}}" type="text/javascript"></script>
@endsection

@section('contenido')
<div class="row">
    <div class="col-lg-12">
        @include('includes.mensaje')
        @include('includes.mensaje-info')
        @include('includes.mensaje-alert')
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Ventas</h3>
                <div class="box-tools pull-right">
                    <a href="{{route('crear_sale')}}" class="btn btn-block btn-success btn-sm">
                        <i class="fa fa-fw fa-plus-circle"></i> Nueva Venta
                    </a>
                </div>
            </div>
            <form class="form-inline float-right">
                <div class="input-group input-group-sm">
                    <input class="form-control" name="search" type="search" placeholder="Buscar Venta" aria-label="Search">
                </div>
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Buscar</button>
            </form>
            <div class="box-tools pull-right">
                <a href="{{route('pdf_sale')}}" class="btn btn-sm btn-primary">
                    <i class="fa fa-fw fa-file-pdf-o"></i> PDF
                </a>
            </div>
            <div class="box-body">
                <table class="table table-striped table-bordered table-hover" id="tabla-data">
                    <thead>
                        <tr>
                            <th>Fecha</th>
                            <th>Articulo</th>
                            <th>Cantidad</th>
                            <th>Monto</th>
                            <th>Usuario</th>
                            <th class="width78"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($datas as $data)
                        <tr>
                            <td>{{$data->date}}</td>
                            <td>
                                @foreach ($articles as $article)
                                    @if($data->article_id == $article->id)
                                        {{$article->name}}
                                    @endif
                                @endforeach
                            </td>
                            <td>{{$data->quantity}}</td>
                            <td>{{number_format($data->amount)}}</td>
                            <td>{{$data->nombre_person}}</td>
                            <td>
                                <form action="{{route('eliminar_sale', ['id' => $data->id])}}" class="d-inline form-eliminar" method="POST">
                                    @csrf @method("delete")
                                    <button type="submit" class="btn-accion-tabla eliminar tooltipsC" title="Eliminar esta venta">
                                        <i class="fa fa-fw fa-trash text-danger"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    <td>
                        @foreach ($monto_t_v as $mont)
                        Monto Total en Varios: {{number_format($mont->total) ?? 0}}
                        @endforeach
                    </td>
                </table>
                {{ $datas->links() }}
            </div>
        </div>
    </div>
</div>
@endsection 