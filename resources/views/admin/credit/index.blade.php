@extends("theme.$theme.layout")
@section('titulo')
Creditos
@endsection

@section("scripts")
<script src="{{asset("assets/pages/scripts/admin/index.js")}}" type="text/javascript"></script>
@endsection

@section('contenido')
<div class="row">
    <div class="col-lg-12">
        @include('includes.mensaje')
        @include('includes.mensaje-info')
        @include('includes.mensaje-alert')
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Creditos</h3>
                <div class="box-tools pull-right">
                    <a href="{{route('crear_credit')}}" class="btn btn-block btn-success btn-sm">
                        <i class="fa fa-fw fa-plus-circle"></i> Nuevo credito
                    </a>
                </div>
            </div>
            <form class="form-inline float-right">
                <label for="">Buscar por fecha</label>
                <br>
                <div class="input-group input-group-sm">
                    <input class="form-control" name="search" type="date" placeholder="Buscar por Fecha" aria-label="Search">
                </div>
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Buscar</button>
            </form>
            <div class="box-body">
                <table class="table table-striped table-bordered table-hover" id="tabla-data">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Monto</th>
                            <th>Descripcion</th>
                            <th>Estado</th>
                            <th>Fecha</th>
                            <th>Usuario</th>
                            <th class="width78"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($datas as $data)
                        <tr>
                            <td>
                                @foreach ($persons as $person)
                                    @if($data->person_id == $person->id)
                                    <dd></dd>
                                        {{$person->full_name}}
                                    @endif
                                @endforeach
                            </td>
                            <td>{{number_format($data->amount)}}</td>
                            <td>{{$data->description}}</td>
                            <td>{{$data->state}}</td>
                            <td>{{$data->date}}</td>
                            <td>{{$data->nombre_person}}</td>
                            <td>
                                <a href="{{route('editar_credit', ['id' => $data->id])}}" class="btn-accion-tabla tooltipsC" title="Editar este registro">
                                    <i class="fa fa-fw fa-pencil"></i>
                                </a>
                                <form action="{{route('eliminar_credit', ['id' => $data->id])}}" class="d-inline form-eliminar" method="POST">
                                    @csrf @method("delete")
                                    <button type="submit" class="btn-accion-tabla eliminar tooltipsC" title="Anular este credito">
                                        <i class="fa fa-fw fa-trash text-danger"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    <td>
                        @foreach ($monto as $mont)
                        Monto Total en Creditos: {{$mont->total}}
                        @endforeach
                    </td>
                </table>
                {{ $datas->links() }}
            </div>
        </div>
    </div>
</div>
@endsection 