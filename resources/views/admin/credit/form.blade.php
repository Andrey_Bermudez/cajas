<div class="form-group">
    <label for="person_id" class="col-lg-3 control-label requerido">Cliente</label>
    <div class="col-lg-8">
      <select name="person_id" id="person_id" class="form-control" required>
        <option value="">Seleccione el Cliente</option>
        @foreach ($persons as $person)
        @if ($person->role_id == 3)
            <option value="{{old('person_id',$person->id ?? '')}}">{{$person->full_name}}</option>
        @endif
        @endforeach
      </select>
    </div>
</div>
<div class="form-group">
    <label for="amount" class="col-lg-3 control-label requerido">Monto</label>
    <div class="col-lg-8">
    <input type="text" name="amount" id="amounts" class="form-control" value="{{old('amount', $data->amount ?? '')}}" required/>
    </div>
</div> 
<div class="form-group">
  <label for="description" class="col-lg-3 control-label">Descripcion</label>
  <div class="col-lg-8">
    <input type="text" name="description" id="description" class="form-control" value="{{old('description', $data->description ?? '')}}" required/>
  </div>
</div>
<div class="form-group">
  <label for="date" class="col-lg-3 control-label">Fecha</label>
  <div class="col-lg-8">
    <input type="date" name="date" id="date" class="form-control" readonly="readonly" value="{{old('date', $fecha->format('Y-m-d'), $data->date ?? '')}}"/>
  </div>
</div>
<div class="form-group">
  <label for="nombre_person" class="col-lg-3 control-label">Usuario</label>
  <div class="col-lg-8">
    <input type="text" name="nombre_person" id="nombre_person" class="form-control" readonly="readonly" value="{{old('nombre_person', session()->get('nombre_person'), $data->nombre_person ?? 'Invitado')}}"/>
  </div>
</div>
<div class="form-group">
  <label for="state" class="col-lg-3 control-label requerido">Estado</label>
  <div class="col-lg-8">
     <select name="state" id="state" class="form-control" value="{{old('state', "Pendiente", $data->state ?? '')}}">
     <option> Pendiente </option>
     <option> Cancelado </option>
    </select>
</div>