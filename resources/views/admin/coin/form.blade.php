<div class="form-group">
    <label for="nombre" class="col-lg-3 control-label requerido">Nombre</label>
    <div class="col-lg-8">
    <input type="text" name="name" id="name" class="form-control" value="{{old('name', $data->name ?? '')}}" required/>
    </div>
</div> 
<div class="form-group">
    <label for="simbolo" class="col-lg-3 control-label requerido">Simbolo</label>
    <div class="col-lg-8">
    <input type="text" name="symbol" id="symbol" class="form-control" value="{{old('symbol', $data->symbol ?? '')}}" required/>
    </div>
</div> 
<div class="form-group">
    <label for="cambio" class="col-lg-3 control-label requerido">Tipo Cambio</label>
    <div class="col-lg-8">
    <input type="text" name="type_change" id="type_change" class="form-control" value="{{old('type_change', $data->type_change ?? '')}}" required/>
    </div>
</div> 
