<div class="form-group">
    <label for="user" class="col-lg-3 control-label requerido">Usuario</label>
    <div class="col-lg-8">
      <input type="text" name="user" id="user" class="form-control"  readonly="readonly" value="{{old('user', $data->user ?? '')}}" required/>
    </div>
</div>
<div class="form-group">
    <label for="password" class="col-lg-3 control-label requerido">Contraseña</label>
    <div class="col-lg-8">
      <input type="password" name="password" id="password" class="form-control"  readonly="readonly" value="{{old('password', $data->password ?? '')}}" required/>
    </div>
</div>
<div class="form-group">
    <label for="full_name" class="col-lg-3 control-label">Nombre Completo</label>
    <div class="col-lg-8">
      <input type="text" name="full_name" id="full_name" class="form-control" readonly="readonly" value="{{old('full_name', $data->full_name ?? '')}}"/>
    </div>
</div>
<div class="form-group">
    <label for="n_telephone" class="col-lg-3 control-label">Telefono</label>
    <div class="col-lg-8">
      <input type="text" name="n_telephone" id="n_telephone" class="form-control" readonly="readonly" value="{{old('n_telephone', $data->n_telephone ?? '')}}"/>
    </div>
</div>
<div class="form-group">
    <label for="mail" class="col-lg-3 control-label">Correo Electronico</label>
    <div class="col-lg-8">
      <input type="email" name="mail" id="mail" class="form-control" readonly="readonly" value="{{old('mail', $data->mail ?? '')}}"/>
    </div>
</div>
<div class="form-group">
    <label for="address" class="col-lg-3 control-label">Direccion</label>
    <div class="col-lg-8">
      <input type="text" name="address" id="address" class="form-control" readonly="readonly" value="{{old('address', $data->address ?? '')}}"/>
    </div>
</div>
<div class="form-group">
    <label for="role_id" class="col-lg-3 control-label requerido">Rol</label>
    <div class="col-lg-8">
      <select name="role_id" id="role_ids" class="form-control" onchange="carg(this);" required>
        <option value="">Seleccione el rol</option>
        @foreach ($roles as $role)
          <option value="{{old('role_id',$role->id ?? '')}}">{{$role->name}}</option>
        @endforeach
      </select>
    </div>
</div>
<div id="limit_credit" class="form-group">
  <label for="limit_credit" class="col-lg-3 control-label">Limite de Credito</label>
  <div class="col-lg-8">
    <input type="text" name="limit_credit" id="limit_credits" class="form-control" disabled value="{{old('limit_credit', $data->limit_credit ?? '')}}"/>
  </div>
</div>