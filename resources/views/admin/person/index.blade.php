@extends("theme.$theme.layout")
@section('titulo')
Personas
@endsection

@section("scripts")
<script src="{{asset("assets/pages/scripts/admin/index.js")}}" type="text/javascript"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
@endsection

@section('contenido')
<div class="row">
    <div class="col-lg-12">
        @include('includes.mensaje')
        @include('includes.mensaje-info')
        @include('includes.mensaje-alert')
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Personas</h3>
                <div class="box-tools pull-right">
                    <a href="{{route('crear_person')}}" class="btn btn-block btn-success btn-sm">
                        <i class="fa fa-fw fa-plus-circle"></i> Nuevo registro
                    </a>
                </div>
            </div>
            <form class="form-inline float-right">
                <div class="input-group input-group-sm">
                    <input class="form-control" name="search" type="search" placeholder="Buscar Persona" aria-label="Search">
                </div>
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Buscar</button>
            </form>
            <div class="box-tools pull-right">
                <a href="{{route('pdf_person')}}" class="btn btn-sm btn-primary">
                    <i class="fa fa-fw fa-file-pdf-o"></i> PDF
                </a>
            </div>
            <div class="box-body">
                <table class="table table-striped table-bordered table-hover" id="tabla-data">
                    <thead>
                        <tr>
                            <th>Usuario</th>
                            <th>Nombre</th>
                            <th>Rol</th>
                            <th class="width78"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($datas as $data)
                        <tr>
                            <td>{{$data->user}}</td>
                            <td>{{$data->full_name}}</td>
                            <td>
                                @foreach ($roles as $role)
                                    @if($data->role_id == $role->id)
                                        {{$role->name}}
                                    @endif
                                @endforeach
                            </td>
                            <td>
                                <a href="{{route('editar_person', ['id' => $data->id])}}" class="btn-accion-tabla tooltipsC" title="Editar este registro">
                                    <i class="fa fa-fw fa-pencil"></i>
                                </a>
                                <form action="{{route('eliminar_person', ['id' => $data->id])}}" class="d-inline form-eliminar" method="POST">
                                    @csrf @method("delete")
                                    <button type="submit" class="btn-accion-tabla eliminar tooltipsC" title="Eliminar este registro">
                                        <i class="fa fa-fw fa-trash text-danger"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection