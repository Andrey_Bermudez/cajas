<div class="form-group">
    <label for="cashier_id" class="col-lg-3 control-label requerido">Cajero</label>
    <div class="col-lg-8">
      <select name="cashier_id" id="cashier_id" class="form-control" required>
        <option value="">Seleccione el Cajero</option>
        @foreach ($persons as $person)
        @if ($person->role_id == 2)
            <option value="{{old('cashier_id',$person->id ?? '')}}">{{$person->full_name}}</option>
        @endif
        @endforeach
      </select>
    </div>
</div>
<div class="form-group">
    <label for="date" class="col-lg-3 control-label">Fecha</label>
    <div class="col-lg-8">
      <input type="date" name="date" id="date" class="form-control" readonly="readonly" value="{{old('date', $fecha->format('Y-m-d'), $data->date ?? '')}}"/>
    </div>
</div>
<div class="form-group">
    <label for="fuel_sale" class="col-lg-3 control-label">Venta Total Combustible</label>
    <div class="col-lg-8">
      <input type="text" name="fuel_sale" id="fuel_sale" class="form-control" value="{{old('fuel_sale', $data->fuel_sale ?? '0')}}"/>
    </div>
</div>
<div class="form-row">
    <div class="col-md-4 mb-3">
      <label for="package_1">Deposito #1</label>
      <input type="text" name="package_1" class="form-control" id="package_1" value="{{old('package_1', $data->package_1  ?? '0')}}"/>
    </div>
    <div class="col-md-4 mb-3">
      <label for="package_2">Deposito #2</label>
      <input type="text" name="package_2" class="form-control" id="package_2" value="{{old('package_2', $data->package_2 ?? '0')}}"/>
    </div>
    <div class="col-md-4 mb-3">
      <label for="package_3">Deposito #3</label>
      <input type="text" name="package_3" class="form-control" id="package_3" value="{{old('package_3', $data->package_3 ?? '0')}}"/>
    </div>
</div>
<div class="form-row">
    <div class="col-md-4 mb-3">
      <label for="package_4">Deposito #4</label>
      <input type="text" name="package_4" class="form-control" id="package_4" value="{{old('package_4', $data->package_4 ?? '0')}}"/>
    </div>
    <div class="col-md-4 mb-3">
      <label for="package_5">Deposito #5</label>
      <input type="text" name="package_5" class="form-control" id="package_5" value="{{old('package_5', $data->package_5 ?? '0')}}"/>
    </div>
    <div class="col-md-4 mb-3">
      <label for="package_6">Deposito #6</label>
      <input type="text" name="package_6" class="form-control" id="package_6" value="{{old('package_6', $data->package_6 ?? '0')}}"/>
    </div>
</div>
<div class="form-row">
    <div class="col-md-4 mb-3">
      <label for="box_total">Caja Total</label>
      <input type="text" name="box_total" class="form-control" id="box_total" value="{{old('box_total', $data->box_total ?? '0')}}"/>
    </div>
    <div class="col-md-4 mb-3">
      <label for="credit">Créditos</label>
      <input type="text" name="credit" class="form-control" id="credit" value="{{old('credit', $data->credit ?? '0')}}"/>
    </div>
    <div class="col-md-4 mb-3">
      <label for="b_bn">Banco Nacional</label>
      <input type="text" name="b_bn" class="form-control" id="b_bn" value="{{old('b_bn', $data->b_bn ?? '0')}}"/>
    </div>
</div>
<div class="form-row">
    <div class="col-md-4 mb-3">
      <label for="b_cr">Banco Costa Rica</label>
      <input type="text" name="b_cr" class="form-control" id="b_cr" value="{{old('b_cr', $data->b_cr ?? '0')}}"/>
    </div>
    <div class="col-md-4 mb-3">
      <label for="b_c">Credomatic</label>
      <input type="text" name="b_c" class="form-control" id="b_c" value="{{old('b_c', $data->b_c ?? '0')}}"/>
    </div>
    <div class="col-md-4 mb-3">
      <label for="b_f">Flota</label>
      <input type="text" name="b_f" class="form-control" id="b_f" value="{{old('b_f', $data->b_f ?? '0')}}"/>
    </div>
</div>
<div class="form-row">
    <div class="col-md-4 mb-3">
      <label for="b_v">Versatec</label>
      <input type="text" name="b_v" class="form-control" id="b_v" value="{{old('b_v', $data->b_v ?? '0')}}"/>
    </div>
    <div class="col-md-4 mb-3">
      <label for="dollars">Dólares</label>
      <input type="text" name="dollars" class="form-control" id="dollars" value="{{old('dollars', $data->dollars ?? '0')}}"/>
    </div>
    <div class="col-md-4 mb-3">
      <label for="checks">Cheques</label>
      <input type="text" name="checks" class="form-control" id="checks" value="{{old('checks', $data->checks ?? '0')}}"/>
    </div>
</div>
<div class="form-row">
    <div class="col-md-4 mb-3">
      <label for="available">Disponibles</label>
      <input type="text" name="available" class="form-control" id="available" value="{{old('available', $data->available ?? '0')}}"/>
    </div>
    <div class="col-md-4 mb-3">
      <label for="token">Vales</label>
      <input type="text" name="token" class="form-control" id="token" value="{{old('token', $data->token ?? '0')}}"/>
    </div>
    <div class="col-md-4 mb-3">
      <label for="previous_box">Caja Anterior</label>
      <input type="text" name="previous_box" class="form-control" id="previous_box" value="{{old('previous_box', $data->previous_box ?? '0')}}"/>
    </div>
</div>