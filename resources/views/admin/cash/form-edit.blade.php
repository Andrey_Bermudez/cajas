<div class="form-group">
    <label for="cashier_id" class="col-lg-3 control-label requerido">Cajero</label>
    <div class="col-lg-8">
      <input type="text" name="cashier_id" id="cashier_id" class="form-control" readonly="readonly" value="{{old('cashier_id', $data->cashier_id ?? '')}}" required/>
    </div>
</div>
<div class="form-group">
    <label for="date" class="col-lg-3 control-label">Fecha</label>
    <div class="col-lg-8">
      <input type="date" name="date" id="date" class="form-control" readonly="readonly" value="{{old('date', $data->date ?? '')}}"/>
    </div>
</div>
<div class="form-group">
    <label for="fuel_sale" class="col-lg-3 control-label">Venta Total Combustible</label>
    <div class="col-lg-8">
      <input type="text" name="fuel_sale" id="fuel_sale" class="form-control" value="{{old('fuel_sale', $data->fuel_sale ?? '')}}"/>
    </div>
</div>
<div class="form-row">
    <div class="col-md-4 mb-3">
      <label for="package_1">Deposito #1</label>
      <input type="text" name="package_1" class="form-control" id="package_1" value="{{old('package_1', $data->package_1 ?? '')}}"/>
    </div>
    <div class="col-md-4 mb-3">
      <label for="package_2">Deposito #2</label>
      <input type="text" name="package_2" class="form-control" id="package_2" value="{{old('package_2', $data->package_2 ?? '')}}"/>
    </div>
    <div class="col-md-4 mb-3">
      <label for="package_3">Deposito #3</label>
      <input type="text" name="package_3" class="form-control" id="package_3" value="{{old('package_3', $data->package_3 ?? '')}}"/>
    </div>
</div>
<div class="form-row">
    <div class="col-md-4 mb-3">
      <label for="package_4">Deposito #4</label>
      <input type="text" name="package_4" class="form-control" id="package_4" value="{{old('package_4', $data->package_4 ?? '')}}"/>
    </div>
    <div class="col-md-4 mb-3">
      <label for="package_5">Deposito #5</label>
      <input type="text" name="package_5" class="form-control" id="package_5" value="{{old('package_5', $data->package_5 ?? '')}}"/>
    </div>
    <div class="col-md-4 mb-3">
      <label for="package_6">Deposito #6</label>
      <input type="text" name="package_6" class="form-control" id="package_6" value="{{old('package_6', $data->package_6 ?? '')}}"/>
    </div>
</div>
<div class="form-row">
    <div class="col-md-4 mb-3">
      <label for="box_total">Caja Total</label>
      <input type="text" name="box_total" class="form-control" id="box_total" value="{{old('box_total', $data->box_total ?? '')}}"/>
    </div>
    <div class="col-md-4 mb-3">
      <label for="credit">Creditos</label>
      <input type="text" name="credit" class="form-control" id="credit" value="{{old('credit', $data->credit ?? '')}}"/>
    </div>
    <div class="col-md-4 mb-3">
      <label for="b_bn">Banco Nacional</label>
      <input type="text" name="b_bn" class="form-control" id="b_bn" value="{{old('b_bn', $data->b_bn ?? '')}}"/>
    </div>
</div>
<div class="form-row">
    <div class="col-md-4 mb-3">
      <label for="b_cr">Banco Costa Rica</label>
      <input type="text" name="b_cr" class="form-control" id="b_cr" value="{{old('b_cr', $data->b_cr ?? '')}}"/>
    </div>
    <div class="col-md-4 mb-3">
      <label for="b_c">Credomatic</label>
      <input type="text" name="b_c" class="form-control" id="b_c" value="{{old('b_c', $data->b_c ?? '')}}"/>
    </div>
    <div class="col-md-4 mb-3">
      <label for="b_f">Flota</label>
      <input type="text" name="b_f" class="form-control" id="b_f" value="{{old('b_f', $data->b_f ?? '')}}"/>
    </div>
</div>
<div class="form-row">
    <div class="col-md-4 mb-3">
      <label for="b_v">Versatec</label>
      <input type="text" name="b_v" class="form-control" id="b_v" value="{{old('b_v', $data->b_v ?? '')}}"/>
    </div>
    <div class="col-md-4 mb-3">
      <label for="dollars">Dólares</label>
      <input type="text" name="dollars" class="form-control" id="dollars" value="{{old('dollars', $data->dollars ?? '')}}"/>
    </div>
    <div class="col-md-4 mb-3">
      <label for="checks">Cheques</label>
      <input type="text" name="checks" class="form-control" id="checks" value="{{old('checks', $data->checks ?? '')}}"/>
    </div>
</div>
<div class="form-row">
    <div class="col-md-4 mb-3">
      <label for="available">Disponibles</label>
      <input type="text" name="available" class="form-control" id="available" value="{{old('available', $data->available ?? '')}}"/>
    </div>
    <div class="col-md-4 mb-3">
      <label for="token">Vales</label>
      <input type="text" name="token" class="form-control" id="token" value="{{old('token', $data->token ?? '')}}"/>
    </div>
    <div class="col-md-4 mb-3">
      <label for="previous_box">Caja Anterior</label>
      <input type="text" name="previous_box" class="form-control" id="previous_box" value="{{old('previous_box', $data->previous_box ?? '')}}"/>
    </div>
</div>