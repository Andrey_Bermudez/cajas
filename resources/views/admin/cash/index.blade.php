@extends("theme.$theme.layout")
@section('titulo')
Cajas
@endsection

@section("scripts")
<script src="{{asset("assets/pages/scripts/admin/index.js")}}" type="text/javascript"></script>
@endsection

@section('contenido')
<div class="row">
    <div class="col-lg-12">
        @include('includes.mensaje')
        @include('includes.mensaje-info')
        @include('includes.mensaje-alert')
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Cajas</h3>
                <div class="box-tools pull-right">
                    <a href="{{route('crear_cash')}}" class="btn btn-block btn-success btn-sm">
                        <i class="fa fa-fw fa-plus-circle"></i> Nueva caja
                    </a>
                </div>
            </div>
            <form class="form-inline float-right">
                <label for="">Buscar por fecha</label>
                <br>
                <div class="input-group input-group-sm">
                    <input class="form-control" name="search" type="date" placeholder="Buscar por Fecha" aria-label="Search">
                </div>
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Buscar</button>
            </form>
            <div class="box-tools pull-right">
                <a href="{{route('pdf_cash')}}" class="btn btn-sm btn-primary">
                    <i class="fa fa-fw fa-file-pdf-o"></i> PDF
                </a>
            </div>
            <div class="box-body">
                <table class="table table-striped table-bordered table-hover" id="tabla-data">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Fecha</th>
                            <th>Cajero</th>
                            <th>Caja Total</th>
                            <th>Diferencia</th>
                            <th class="width78"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($datas as $data)
                        <tr>
                            <td>{{$data->id}}</td>
                            <td>{{$data->date}}</td>
                            <td>
                                @foreach ($persons as $person)
                                    @if($data->cashier_id == $person->id)
                                        {{$person->full_name}}
                                    @endif
                                @endforeach
                            </td>
                            <td>{{number_format($data->box_total)}}</td>
                            <td>
                                {{number_format($data->difference)}}
                            </td>
                            <td>
                                <a href="{{route('editar_cash', ['id' => $data->id])}}" class="btn-accion-tabla tooltipsC" title="Editar este registro">
                                    <i class="fa fa-fw fa-pencil"></i>
                                </a>
                                <!--<form action="{{route('eliminar_cash', ['id' => $data->id])}}" class="d-inline form-eliminar" method="POST">
                                    @csrf @method("delete")
                                    <button type="submit" class="btn-accion-tabla eliminar tooltipsC" title="Eliminar esta caja">
                                        <i class="fa fa-fw fa-trash text-danger"></i>
                                    </button>
                                </form>-->
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection 