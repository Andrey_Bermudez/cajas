<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route; 

Route::get('/','InicioController@index')->name('inicio');
//Route::get('/', 'Seguridad\LoginController@index')->name('inicio'); 
Route::get('seguridad/login', 'Seguridad\LoginController@index')->name('login');
Route::post('seguridad/login', 'Seguridad\LoginController@login')->name('login_post');
Route::get('seguridad/logout', 'Seguridad\LoginController@logout')->name('logout');
Route::post('ajax-sesion', 'AjaxController@setSession')->name('ajax')->middleware('auth');
Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => ['auth', 'superadmin']], function () {
    Route::get('','AdminController@index');
    /*Rutas Permiso*/
    Route::get('permit', 'PermitController@index')->name('permit');
    Route::get('permit/crear', 'PermitController@crear')->name('crear_permit');
    Route::post('permit', 'PermitController@guardar')->name('guardar_permit');
    Route::get('permit/{id}/editar', 'PermitController@editar')->name('editar_permit');
    Route::put('permit/{id}', 'PermitController@actualizar')->name('actualizar_permit');
    Route::delete('permit/{id}', 'PermitController@eliminar')->name('eliminar_permit');
    /*Rutas Menu*/
    Route::get('menu', 'MenuController@index')->name('menu');
    Route::get('menu/crear', 'MenuController@crear')->name('crear_menu');
    Route::post('menu', 'MenuController@guardar')->name('guardar_menu');
    Route::get('menu/{id}/editar', 'MenuController@editar')->name('editar_menu');
    Route::put('menu/{id}', 'MenuController@actualizar')->name('actualizar_menu');
    Route::get('menu/{id}/eliminar', 'MenuController@eliminar')->name('eliminar_menu');
    Route::post('menu/guardar-orden', 'MenuController@guardarOrden')->name('guardar_orden');
    /*Rutas ROL*/
    Route::get('role', 'RoleController@index')->name('role');
    Route::get('role/crear', 'RoleController@crear')->name('crear_role');
    Route::post('role', 'RoleController@guardar')->name('guardar_role');
    Route::get('role/{id}/editar', 'RoleController@editar')->name('editar_role');
    Route::put('role/{id}', 'RoleController@actualizar')->name('actualizar_role');
    Route::delete('role/{id}', 'RoleController@eliminar')->name('eliminar_role');
    /*RUTAS MENU_ROL*/
    Route::get('menu-role', 'MenuRoleController@index')->name('menu_role');
    Route::post('menu-role', 'MenuRoleController@guardar')->name('guardar_menu_role');
    /*Rutas Person*/
    Route::get('person', 'PersonController@index')->name('person');
    Route::get('person/crear', 'PersonController@crear')->name('crear_person');
    Route::post('person', 'PersonController@guardar')->name('guardar_person');
    Route::get('person/{id}/editar', 'PersonController@editar')->name('editar_person');
    Route::put('person/{id}', 'PersonController@actualizar')->name('actualizar_person');
    Route::delete('person/{id}', 'PersonController@eliminar')->name('eliminar_person');
    Route::get('/pdf-person', 'PDFController@PDFPerson')->name('pdf_person');
    /*Rutas Coin*/
    Route::get('coin', 'CoinController@index')->name('coin');
    Route::get('coin/crear', 'CoinController@crear')->name('crear_coin');
    Route::post('coin', 'CoinController@guardar')->name('guardar_coin');
    Route::get('coin/{id}/editar', 'CoinController@editar')->name('editar_coin');
    Route::put('coin/{id}', 'CoinController@actualizar')->name('actualizar_coin');
    Route::delete('coin/{id}', 'CoinController@eliminar')->name('eliminar_coin');
    /*Rutas Vales Paybill*/
    Route::get('paybill', 'PaybillController@index')->name('paybill');
    Route::get('paybill/crear', 'PaybillController@crear')->name('crear_paybill');
    Route::post('paybill', 'PaybillController@guardar')->name('guardar_paybill');
    Route::get('paybill/{id}/editar', 'PaybillController@editar')->name('editar_paybill');
    Route::put('paybill/{id}', 'PaybillController@actualizar')->name('actualizar_paybill');
    Route::delete('paybill/{id}', 'PaybillController@eliminar')->name('eliminar_paybill');
    /*Rutas Credito*/
    Route::get('credit', 'CreditController@index')->name('credit');
    Route::get('credit/crear', 'CreditController@crear')->name('crear_credit');
    Route::post('credit', 'CreditController@guardar')->name('guardar_credit');
    Route::get('credit/{id}/editar', 'CreditController@editar')->name('editar_credit');
    Route::put('credit/{id}', 'CreditController@actualizar')->name('actualizar_credit');
    Route::delete('credit/{id}', 'CreditController@eliminar')->name('eliminar_credit');
    /*Rutas Dia*/
    Route::get('day', 'DayController@index')->name('day');
    Route::get('day/crear', 'DayController@crear')->name('crear_day');
    Route::post('day', 'DayController@guardar')->name('guardar_day');
    Route::get('day/{id}/editar', 'DayController@editar')->name('editar_day');
    Route::put('day/{id}', 'DayController@actualizar')->name('actualizar_day');
    Route::delete('day/{id}', 'DayController@eliminar')->name('eliminar_day');
    /*Rutas Credito Cliente*/
    Route::get('c_credit', 'C_CreditController@index')->name('c_credit');
    /*Rutuas Proveedor*/
    Route::get('brand', 'BrandController@index')->name('brand');
    Route::get('brand/crear', 'BrandController@crear')->name('crear_brand');
    Route::post('brand', 'BrandController@guardar')->name('guardar_brand');
    Route::get('brand/{id}/editar', 'BrandController@editar')->name('editar_brand');
    Route::put('brand/{id}', 'BrandController@actualizar')->name('actualizar_brand');
    Route::delete('brand/{id}', 'BrandController@eliminar')->name('eliminar_brand');
    /*Rutuas Articulos*/
    Route::get('article', 'ArticlesController@index')->name('article');
    Route::get('article/crear', 'ArticlesController@crear')->name('crear_article');
    Route::post('article', 'ArticlesController@guardar')->name('guardar_article');
    Route::get('article/{id}/editar', 'ArticlesController@editar')->name('editar_article');
    Route::put('article/{id}', 'ArticlesController@actualizar')->name('actualizar_article');
    Route::delete('article/{id}', 'ArticlesController@eliminar')->name('eliminar_article');
    /*Rutuas Venta*/
    Route::get('sale', 'SaleController@index')->name('sale');
    Route::get('sale/crear', 'SaleController@crear')->name('crear_sale');
    Route::post('sale', 'SaleController@guardar')->name('guardar_sale');
    Route::get('sale/{id}/editar', 'SaleController@editar')->name('editar_sale');
    Route::put('sale/{id}', 'SaleController@actualizar')->name('actualizar_sale');
    Route::delete('sale/{id}', 'SaleController@eliminar')->name('eliminar_sale');
    Route::get('/pdf-sale', 'PDFController@PDFSale')->name('pdf_sale');
    /*Rutuas Caja*/
    Route::get('cash', 'CashController@index')->name('cash');
    Route::get('cash/crear', 'CashController@crear')->name('crear_cash');
    Route::post('cash', 'CashController@guardar')->name('guardar_cash');
    Route::get('cash/{id}/editar', 'CashController@editar')->name('editar_cash');
    Route::put('cash/{id}', 'CashController@actualizar')->name('actualizar_cash');
    Route::delete('cash/{id}', 'CashController@eliminar')->name('eliminar_cash');
    Route::get('/pdf-cash', 'PDFController@PDFCash')->name('pdf_cash');
});
