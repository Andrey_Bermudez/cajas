<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PersonAdministradorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('person')->insert([
            'user' => 'admin',
            'password' => bcrypt('123'),
            'full_name' => 'Administrador',
            'n_telephone' => '12345678',
            'mail' => 'admin@gmail.com',
            'address' => 'admin',
            'limit_credit' => 0,
            'role_id' => '1'
        ]);
    }
}
