<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('person', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user')->unique();
            $table->string('password')->bcrypt();
            $table->string('full_name');
            $table->string('n_telephone');
            $table->string('mail');
            $table->string('address');
            $table->float('limit_credit');
            $table->unsignedInteger('role_id');
            $table->foreign('role_id', 'fk_personrole_role')->references('id')->on('role')->onDelete('restrict')->onUpdate('restrict');
            $table->timestamps();
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_spanish_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('person');
    }
}
