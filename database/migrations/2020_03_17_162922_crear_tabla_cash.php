<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTablaCash extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cash', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('cashier_id');
            $table->foreign('cashier_id', 'fk_person_cash')->references('id')->on('person')->onDelete('restrict')->onUpdate('restrict');
            $table->date('date');
            $table->float('package_1', 50,2)->default(0.00);
            $table->float('package_2', 50,2)->default(0.00);
            $table->float('package_3', 50,2)->default(0.00);
            $table->float('package_4', 50,2)->default(0.00);
            $table->float('package_5', 50,2)->default(0.00);
            $table->float('package_6', 50,2)->default(0.00);
            $table->float('box_total', 50,2)->default(0.00);
            $table->float('credit', 50,2)->default(0.00);
            $table->float('b_bn', 50,2)->default(0.00);
            $table->float('b_cr', 50,2)->default(0.00);
            $table->float('b_c', 50,2)->default(0.00);
            $table->float('b_f', 50,2)->default(0.00);
            $table->float('b_v', 50,2)->default(0.00);
            $table->float('dollars', 50,2)->default(0.00);
            $table->float('checks', 50,2)->default(0.00);
            $table->float('available', 50,2)->default(0.00);
            $table->float('token', 50,2)->default(0.00);
            $table->float('previous_box', 50,2)->default(0.00);
            $table->float('fuel_sale', 50,2)->default(0.00);
            $table->float('difference', 50,2)->default(0.00);
            $table->timestamps();
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_spanish_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cash');
    }
}
